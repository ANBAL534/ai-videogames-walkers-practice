using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class comanderSpawn : MonoBehaviour
{
    public GameObject spawnComanderPrefab;
    public List<GameObject> spawnsComander = new List<GameObject>();

    public int numComander = 2;

    // Start is called before the first frame update
    void OnEnable() {

        var pos1 = new Vector3(1000, 1000, 1000);

        for (int i=0; i<numComander; i++)
        {
            var pos = new Vector3(Random.Range(-30, 30), 1.1f, Random.Range(-30, 30));
            int antiLoop = 0;
            while(Mathf.Abs(Vector3.Distance(pos,pos1)) < 45f && antiLoop < 10) {
                pos = new Vector3(Random.Range(-30, 30), 1.1f, Random.Range(-30, 30));
                antiLoop++;
            }

            var commander = Instantiate(spawnComanderPrefab, pos, Quaternion.identity);
            spawnsComander.Add(commander);
            commander.GetComponent<MinionCommanderBehaviour>().team = i + 1;
            pos1 = pos;
        }
    }
}
