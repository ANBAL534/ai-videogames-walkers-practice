using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class StarSpawn : MonoBehaviour
{  

    public GameObject spawnStarPrefab;
    public List<GameObject> spawnsStar = new List<GameObject>();

    public int numStar = 1;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numStar; i++)
        {
            var pos = new Vector3(Random.Range(-30, 30), 1.5f, Random.Range(-30, 30));
            int antiLoop = 0;
            while (IsComanderNear(pos) && antiLoop < 10)
            {
                pos = new Vector3(Random.Range(-30, 30), 1.5f, Random.Range(-30, 30));
                antiLoop++;
            }
            spawnsStar.Add(Instantiate(spawnStarPrefab, pos, Quaternion.identity));
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i<spawnsStar.Count; i++)
        {
            if (spawnsStar[i] != null)
            {
                return;
            }
        }
        Start();
    }

    public bool IsComanderNear(Vector3 p)
    {
        var comanders = GameObject.FindGameObjectsWithTag("Player");

        foreach(var comander in comanders){
            if (Mathf.Abs(Vector3.Distance(p, comander.transform.position)) < 6)
            {
                return true;
            }
        }
        return false;
    }
}
