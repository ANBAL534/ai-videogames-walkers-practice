using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using UnityEngine;

public class MinionCommanderBehaviour : MonoBehaviour
{
    public GameObject effect;
    
    public float matrixResolution = 1f;
    public float fromWorldProbe = -100f;
    public float toWorldProbe = 100f;
    [Range(1, 5)]public int team;

    private Vector3 invalidV3 = new Vector3(-10000, -10000, -10000);
    private Vector3[][] worldMatrix;
    private int[][] map;
    private List<MinionBehaviour> ownMinions = new List<MinionBehaviour>();
    private List<MinionCommanderBehaviour> enemyCommanders = new List<MinionCommanderBehaviour>();
    
    // Start is called before the first frame update
    void Start()
    {
        // Map to world translation matrix
        worldMatrix = new Vector3[Mathf.CeilToInt(Mathf.Abs(toWorldProbe / matrixResolution))*2][];
        for (int i = 0; i < worldMatrix.Length; i++)
            worldMatrix[i] = new Vector3[Mathf.CeilToInt(Mathf.Abs(toWorldProbe / matrixResolution))*2];

        // initialize map matrix
        map = new int[Mathf.CeilToInt(Mathf.Abs(toWorldProbe / matrixResolution))*2][];
        for (int i = 0; i < map.Length; i++)
            map[i] = new int[Mathf.CeilToInt(Mathf.Abs(toWorldProbe / matrixResolution))*2];

        // Find and filter the enemy commanders
        foreach (var commanderBehaviour in FindObjectsOfType<MinionCommanderBehaviour>())
        {
            if (commanderBehaviour.team != team) 
                enemyCommanders.Add(commanderBehaviour);
        }
    }

    void FixedUpdate()
    {
        // Update world and map matrix
        int i = 0, j = 0;
        for (float z = fromWorldProbe; z < toWorldProbe; z+=matrixResolution)
        {
            for (float x = fromWorldProbe; x < toWorldProbe; x+=matrixResolution)
            {
                worldMatrix[i][j] = invalidV3;
                map[i][j] = 1;  // Wall for the Astar algorithm
                
                RaycastHit hit;
                if (Physics.Raycast(new Vector3(x, 50, z), Vector3.down, out hit, 100))
                {
                    if (hit.transform.CompareTag("Terrain") || hit.transform.CompareTag("Player") || hit.transform.CompareTag("Minion"))
                    {
                        worldMatrix[i][j] = hit.point;
                        map[i][j] = 0;
                    }
                }
        
                j++;
            }
        
            i++;
            j = 0;
        }
        
        SetMinionObjectives();
        SetCommanderObjective();
    }

    private void SetMinionObjectives()
    {
        ownMinions = new List<MinionBehaviour>();
        // Find and filter the minions to get only our own
        foreach (MinionBehaviour minionBehaviour in FindObjectsOfType<MinionBehaviour>())
        {
            if (minionBehaviour.team == team) 
                ownMinions.Add(minionBehaviour);
        }
        
        foreach (var ownMinion in ownMinions)
        {
            var pathToObj = FindPath(ownMinion.transform.position, enemyCommanders[0].transform.position);
            ownMinion.GetComponent<DinamicMovement>().UpdateWaypoint(pathToObj.Count > 3 ? pathToObj[1] : enemyCommanders[0].transform.position);
        }
    }

    private void SetCommanderObjective()
    {
        var point = FindObjectOfType<PointBehaviour>().transform.position;
        var pathToObj = FindPath(transform.position, point);
        GetComponent<DinamicMovement>().UpdateWaypoint(pathToObj.Count > 3 ? pathToObj[1] : point);
    }

    public List<Vector3> FindPath(Vector3 from, Vector3 to)
    {
        Vector2Int fromMapPoint, toMapPoint;
        fromMapPoint = GetClosestMapPointFromVector3(from);
        toMapPoint = GetClosestMapPointFromVector3(to);

        var path = new Astar(Astar.ConvertToBoolArray(map), fromMapPoint, toMapPoint).Result;
        return GetVector3Path(path);
    }

    private Vector2Int GetClosestMapPointFromVector3(Vector3 v)
    {
        var closestPosition = Vector2Int.zero;
        var closestDistance = 1000000.0f;
        for (int i = 0; i < map.Length; i++)
        {
            for (int j = 0; j < map[i].Length; j++)
            {
                if (Mathf.Abs(Vector3.Distance(v, worldMatrix[i][j])) < closestDistance)
                {
                    closestDistance = Mathf.Abs(Vector3.Distance(v, worldMatrix[i][j]));
                    closestPosition = new Vector2Int(j, i);
                }
            }
        }

        return closestPosition;
    }

    private List<Vector3> GetVector3Path(List<Vector2Int> vector2IntList)
    {
        var result = new List<Vector3>();
        
        foreach (var vector2Int in vector2IntList)
        {
            result.Add(worldMatrix[vector2Int.y][vector2Int.x]);
        }

        return result;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(effect, transform.position, Quaternion.identity);
        collision.rigidbody.AddExplosionForce(200000 * Time.deltaTime, collision.contacts[0].point, 2000);
    }
    
}