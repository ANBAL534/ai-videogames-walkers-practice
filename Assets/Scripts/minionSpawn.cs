using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minionSpawn : MonoBehaviour
{
    public GameObject spawnMinionPrefab;

    public int size = 3;

    public int numMinion = 4;

    // Start is called before the first frame update
    void Start()
    {
        //Se deberia poner un numero random de minion?
        SpawnMinion();
    }

    public void SpawnMinion()
    {
        for (int i = 0; i < numMinion; i++)
        {
            Vector3 pos = new Vector3(Random.Range(transform.position.x + size, transform.position.x - size), 0.55f, Random.Range(transform.position.z + size, transform.position.z - size));
            Instantiate(spawnMinionPrefab, pos, Quaternion.identity).GetComponent<MinionBehaviour>().team = GetComponent<MinionCommanderBehaviour>().team;
        }
    }
}

