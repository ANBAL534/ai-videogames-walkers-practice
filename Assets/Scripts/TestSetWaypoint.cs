using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSetWaypoint : MonoBehaviour
{
    public DinamicMovement dm;
    public Transform waypoint;

    private void Start()
    {
        waypoint = transform;
    }

    // Start is called before the first frame update
    void Update()
    {
        dm.UpdateWaypoint(waypoint.position);
    }
}
