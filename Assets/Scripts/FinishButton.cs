using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishButton : MonoBehaviour
{
    public Button finishButton;
    void Start()
    {
        finishButton.onClick.AddListener(Task);
    }

    void Task()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
