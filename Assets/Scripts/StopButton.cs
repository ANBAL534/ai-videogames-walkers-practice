using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopButton : MonoBehaviour
{
    public Button button;
    public bool isPausing = false;
    void Start()
    {
        button.onClick.AddListener(Task);
    }

    void Task()
    {
        if(isPausing)
        {
            Time.timeScale = 1;
            isPausing = false;
        }
        else
        {
            Time.timeScale = 0.05f;
            isPausing = true;
        }

    }
}
