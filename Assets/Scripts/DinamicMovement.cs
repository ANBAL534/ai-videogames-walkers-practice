using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class DinamicMovement : MonoBehaviour
{
    private Vector3 waypoint; //objetivo
    public float speed;
    private Rigidbody rb;
    private Vector3 diference;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        waypoint = transform.position; // + Vector3.forward *2; //hacia delante 
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(Vector3.Distance(waypoint, transform.position)) >= 1.5f)
        {
            diference = waypoint - transform.position;
            rb.AddForce(diference.x * speed, diference.y * speed, diference.z * speed);

            Debug.DrawLine(transform.position, waypoint, Color.red);
        }
    }

    public void UpdateWaypoint(Vector3 w)
    {
        waypoint = w;
    }
}