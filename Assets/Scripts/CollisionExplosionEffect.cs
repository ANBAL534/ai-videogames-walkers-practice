using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionExplosionEffect : MonoBehaviour
{
    public GameObject explosionEffect;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(explosionEffect, collision.contacts[0].point, Quaternion.identity);
    }
}
