using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            MoveForward();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            rb.AddForce(-transform.forward);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            rb.AddForce(transform.right);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            rb.AddForce(-transform.right);
        }
    }

    public void MoveForward()
    {
        rb.AddForce(transform.forward);
    }
}
