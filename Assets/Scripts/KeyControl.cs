using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyControl : MonoBehaviour
{
    private Vector3 waypoint;//objetivo
    private DinamicMovement d;
    // Start is called before the first frame update
    void Start()//no hace falta porque sino empezaria moviendose 
    {
        d = GetComponent<DinamicMovement>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            d.UpdateWaypoint(transform.position + Vector3.forward * 2);
        }
        if (Input.GetKey(KeyCode.S))
        {
            d.UpdateWaypoint(transform.position + Vector3.forward * -2);

        }
        if (Input.GetKey(KeyCode.A))
        {
            d.UpdateWaypoint(waypoint = transform.position + Vector3.left * 2);
        }
        if (Input.GetKey(KeyCode.D))
        {
            d.UpdateWaypoint(transform.position + Vector3.right * 2);
        }

    }
}
