using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointBehaviour : MonoBehaviour
{

    public GameObject effect;

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(effect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        collision.rigidbody.AddExplosionForce(200000 * Time.deltaTime, collision.contacts[0].point, 2000);
    }
}
